package corso.lez25.SpringCartaFedelta.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="persona")
public class Persona {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="personaID")
	private int id;
	
	@Column
	private String nome;
	@Column
	private String cognome;
	@Column(name="codice_fiscale")
	private String cod_fis;
	
	@JsonBackReference
	@OneToMany(mappedBy = "proprietario", 
			cascade = CascadeType.ALL, 
			fetch = FetchType.LAZY)			//FetchType.LAZY
	private List<Carta> elencoCarte;
	
	public Persona() {
		
	}
	
	public Persona(String nome, String cognome, String cod_fis) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.cod_fis = cod_fis;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getCod_fis() {
		return cod_fis;
	}
	public void setCod_fis(String cod_fis) {
		this.cod_fis = cod_fis;
	}

	public List<Carta> getElencoCarte() {
		return elencoCarte;
	}

	public void setElencoCarte(List<Carta> elencoCarte) {
		this.elencoCarte = elencoCarte;
	}

	@Override
	public String toString() {
		return "Persona [id=" + id + ", nome=" + nome + ", cognome=" + cognome + ", cod_fis=" + cod_fis
				+ "]";
	}
	
	public String stampaDettaglioPersona() {
		return "Persona [id=" + id + ", nome=" + nome + ", cognome=" + cognome + ", cod_fis=" + cod_fis
				+ ", elencoCarte=" + elencoCarte + "]";
	}
	
}
