package corso.lez25.SpringCartaFedelta.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import corso.lez25.SpringCartaFedelta.models.Persona;
import corso.lez25.SpringCartaFedelta.services.PersonaService;

@RestController
@RequestMapping("/persona")
@CrossOrigin("*")
public class PersonaController {

	@Autowired
	private PersonaService service;

	@PostMapping("/inserisci")
	public Persona inserisciOggetto(@RequestBody Persona obj) {
		return service.insert(obj);
	}
	
	@GetMapping("/{id}")
	public Persona ricercaOggettoPerId(@PathVariable(name="id") int varId) {
		return service.findById(varId);
	}
	
	@GetMapping("/")
	public List<Persona> ricercaTuttiOggetti(){
		return service.findAll();
	}
	
	@DeleteMapping("/{id}")
	public boolean eliminaOggetto(@PathVariable(name="id") int varId) {
		return service.delete(varId);
	}
	
	@PutMapping("/modifica")
	public boolean modificaOggetto(@RequestBody Persona obj) {
		return service.update(obj);
	}
	
}
