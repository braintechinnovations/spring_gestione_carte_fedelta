package corso.lez25.SpringCartaFedelta.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import corso.lez25.SpringCartaFedelta.models.Persona;
import corso.lez25.SpringCartaFedelta.repos.DataAccessRepo;

@Service
public class PersonaService implements DataAccessRepo<Persona>{

	@Autowired
	private EntityManager ent_man;
	
	private Session getSessione() {
		return ent_man.unwrap(Session.class);
	}

	public Persona insert(Persona obj) {
		
		Persona temp = new Persona();
		temp.setNome(obj.getNome());
		temp.setCognome(obj.getCognome());
		temp.setCod_fis(obj.getCod_fis());
		
		Session sessione = getSessione();	
		
		try {
			sessione.save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		} finally {
			sessione.close();						//Superfluo perché gestito da Spring
		}
		
	}
	
	public Persona findById(int varId) {
		
		Session sessione = getSessione();
		
		return (Persona) sessione.createCriteria(Persona.class)
				.add(Restrictions.eqOrIsNull("id", varId))
				.uniqueResult();
		
//		CriteriaBuilder cb = sessione.getCriteriaBuilder();
//		CriteriaQuery<Prodotto> cr = cb.createQuery(Prodotto.class);
//		Root<Prodotto> prodotto = cr.from(Prodotto.class);
//		cr.select(prodotto).where(cb.gt(prodotto.get("id"), varId));
//		
//
//		Query<Prodotto> query = sessione.createQuery(cr);
//		
//
//        return (Prodotto) query.getResultList();
		
		}
	
	public List<Persona> findAll(){
		
		Session sessione = getSessione();
//		return sessione.createCriteria(Prodotto.class).list();
		
		CriteriaQuery<Persona> cq = sessione.getCriteriaBuilder().createQuery(Persona.class);
        cq.from(Persona.class);
        List<Persona> prodotti = sessione.createQuery(cq).getResultList();
        return prodotti;
	}
	
	@Transactional
	public boolean delete(int varId) {
		
		Session sessione = getSessione();

		try {
			
			Persona temp = sessione.load(Persona.class, varId);
			sessione.delete(temp);
			sessione.flush();
			
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			
			return false;
		} finally {
			sessione.close();
		}
	}
	
	@Transactional
	public boolean update(Persona obj) {
		
		Session sessione = getSessione();
		
		try {
			
			Persona temp = sessione.load(Persona.class, obj.getId());
			
			if(obj.getNome() != null)
				temp.setNome(obj.getNome());
			if(obj.getCognome() != null)
				temp.setCognome(obj.getCognome());
			if(obj.getCod_fis() != null)
				temp.setCod_fis(obj.getCod_fis());
			
			sessione.save(temp);
			sessione.flush();
			
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			
			return false;
		} finally {
			sessione.close();
		}
		
	}
	
}
