package corso.lez25.SpringCartaFedelta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCartaFedeltaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCartaFedeltaApplication.class, args);
	}

}
