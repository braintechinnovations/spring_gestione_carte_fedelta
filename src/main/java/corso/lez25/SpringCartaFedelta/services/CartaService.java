package corso.lez25.SpringCartaFedelta.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import corso.lez25.SpringCartaFedelta.models.Carta;
import corso.lez25.SpringCartaFedelta.models.Persona;
import corso.lez25.SpringCartaFedelta.repos.DataAccessRepo;

@Service
public class CartaService implements DataAccessRepo<Carta>{

	@Autowired
	private EntityManager ent_man;
	
	private Session getSessione() {
		return ent_man.unwrap(Session.class);
	}
	
	/**
	 * Metodo per l'associazione di una carta ad un proprietario
	 * @param carId	Identificatore intero della carta
	 * @param proId Identificatore intero del proprietario
	 * @return
	 */
	@Transactional
	public boolean associa(int carId, int proId) {

		Session sessione = getSessione();
		try {
			Persona objProp = sessione.load(Persona.class, proId);
			Carta objCart = sessione.load(Carta.class, carId);
			
			objCart.setProprietario(objProp);
			sessione.save(objCart);
			sessione.flush();;
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
	}
	
	//Questo metodo non si trova nel DataAccessRepo a causa della sua firma! ;)
	public Carta insert(int perId, Carta obj) {
		
		Carta temp = new Carta();
		temp.setNumero(obj.getNumero());
		temp.setNegozio(obj.getNegozio());
		
		Session sessione = getSessione();	
		
		try {
			Persona objProp = sessione.load(Persona.class, perId);
			
			temp.setProprietario(objProp);
			sessione.save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		} finally {
			sessione.close();						//Superfluo perché gestito da Spring
		}
	}

	public Carta insert(Carta obj) {
		
		Carta temp = new Carta();
		temp.setNumero(obj.getNumero());
		temp.setNegozio(obj.getNegozio());
		
		Session sessione = getSessione();	
		
		try {
			sessione.save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		} finally {
			sessione.close();						//Superfluo perché gestito da Spring
		}
		
	}
	
	public Carta findById(int varId) {
		
		Session sessione = getSessione();
		
		return (Carta) sessione.createCriteria(Carta.class)
				.add(Restrictions.eqOrIsNull("id", varId))
				.uniqueResult();
		
//		CriteriaBuilder cb = sessione.getCriteriaBuilder();
//		CriteriaQuery<Prodotto> cr = cb.createQuery(Prodotto.class);
//		Root<Prodotto> prodotto = cr.from(Prodotto.class);
//		cr.select(prodotto).where(cb.gt(prodotto.get("id"), varId));
//		
//
//		Query<Prodotto> query = sessione.createQuery(cr);
//		
//
//        return (Prodotto) query.getResultList();
		
		}
	
	public List<Carta> findAll(){
		
		Session sessione = getSessione();
//		return sessione.createCriteria(Prodotto.class).list();
		
		CriteriaQuery<Carta> cq = sessione.getCriteriaBuilder().createQuery(Carta.class);
        cq.from(Carta.class);
        List<Carta> prodotti = sessione.createQuery(cq).getResultList();
        return prodotti;
	}
	
	@Transactional
	public boolean delete(int varId) {
		
		Session sessione = getSessione();

		try {
			
			Carta temp = sessione.load(Carta.class, varId);
			sessione.delete(temp);
			sessione.flush();
			
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			
			return false;
		} finally {
			sessione.close();
		}
	}
	
	@Transactional
	public boolean update(Carta obj) {
		
		Session sessione = getSessione();
		
		try {
			
			Carta temp = sessione.load(Carta.class, obj.getId());
			
			if(obj.getNumero() != null)
				temp.setNumero(obj.getNumero());
			if(obj.getNegozio() != null)
				temp.setNegozio(obj.getNegozio());
			if(obj.getProprietario() != null)
				temp.setProprietario(obj.getProprietario());
			
			sessione.save(temp);
			sessione.flush();
			
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			
			return false;
		} finally {
			sessione.close();
		}
		
	}
	
	/**
	 * Cerco tutte le carte che appartengono ad un singolo proprietario
	 * @param propId
	 * @return
	 */
	public List<Carta> findByProprietario(int propId) {
		
		Session sessione = getSessione();
		
		return sessione.createCriteria(Carta.class)
				.add(Restrictions.eqOrIsNull("proprietario.id", propId))
				.list();
		
	}
	
}
