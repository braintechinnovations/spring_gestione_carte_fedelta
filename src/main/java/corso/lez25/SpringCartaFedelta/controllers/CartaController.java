package corso.lez25.SpringCartaFedelta.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import corso.lez25.SpringCartaFedelta.models.Carta;
import corso.lez25.SpringCartaFedelta.services.CartaService;

@RestController
@RequestMapping("/carta")
@CrossOrigin("*")
public class CartaController {

	@Autowired
	private CartaService service;

	@PostMapping("/inserisci/{proprietario_id}")
	public Carta inserisciCartaConProprietario(
			@PathVariable (name="proprietario_id") int idProp, 
			@RequestBody Carta obj) {
		
		if(idProp == 0) {
			//TODO: Response con errore
		}
		if(obj.getNegozio().isEmpty() || obj.getNumero().isEmpty()) {
			//TODO: Response con errore
		}
		
		return service.insert(idProp, obj);
	}
	
	@PostMapping("/inserisci")
	public Carta inserisciOggetto(@RequestBody Carta obj) {
		return service.insert(obj);
	}
	
	
	@GetMapping("/associa/{carta_id}/{proprietario_id}")
	public boolean associaCartaProprietario(@PathVariable int carta_id, @PathVariable int proprietario_id) {
		return service.associa(carta_id, proprietario_id);
	}
	
	@GetMapping("/{id}")
	public Carta ricercaOggettoPerId(@PathVariable(name="id") int varId) {
		return service.findById(varId);
	}
	
	@GetMapping("/")
	public List<Carta> ricercaTuttiOggetti(){
		return service.findAll();
	}
	
	@DeleteMapping("/{id}")
	public boolean eliminaOggetto(@PathVariable(name="id") int varId) {
		return service.delete(varId);
	}
	
	@PutMapping("/modifica")
	public boolean modificaOggetto(@RequestBody Carta obj) {
		return service.update(obj);
	}
	
	/**
	 * Aggiunta finale
	 */
	@GetMapping("/proprietario/{persona_id}")
	public List<Carta> getCartePerProprietario(@PathVariable(name="persona_id") int proId){
		return service.findByProprietario(proId);
	}
}
