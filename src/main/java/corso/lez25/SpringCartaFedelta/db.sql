DROP DATABASE IF EXISTS carte_fedelta_hibernate;
CREATE DATABASE carte_fedelta_hibernate;
USE carte_fedelta_hibernate;

CREATE TABLE persona (
	personaID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(250),
    cognome VARCHAR(250),
    codice_fiscale VARCHAR(16) UNIQUE NOT NULL
);

CREATE TABLE carta_fedelta(
	cartaID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    negozio VARCHAR(250),
    numero_carta VARCHAR(200),
    persona_rif INTEGER,
    FOREIGN KEY (persona_rif) REFERENCES persona(personaID) ON DELETE CASCADE
);

SELECT * FROM persona;
SELECT * FROM carta_fedelta;